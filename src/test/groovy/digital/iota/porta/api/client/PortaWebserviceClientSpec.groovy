/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api.client

import spock.lang.Specification


/**
 *
 */
class PortaWebserviceClientSpec extends Specification {

    def "Creating a client"() {
        when: "A client is instantiated with a URI"
        def client = new PortaWebserviceClient('http://foo')

        then: "no exception is thrown"
        noExceptionThrown()
    }
}