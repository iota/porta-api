/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api.client

import groovy.transform.CompileStatic
import groovyx.net.http.HTTPBuilder
import digital.iota.porta.api.internal.JsonApi

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * @author Schalk W. Cronjé
 */
@CompileStatic
abstract class AbstractPortaWebserviceClient {

    static final DateTimeFormatter DATETIME_PATTERN = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss')

    protected AbstractPortaWebserviceClient(final String baseUri) {
        api = new JsonApi(baseUri)
    }

    /** Provides access to underlying HTTP Client
     *
     */
    HTTPBuilder getHttpClient() {
        api.restClient
    }

    /** Set the timezone of the remote PortaOne server.
     *
     * By default the timezone is set to {@code 'Z'}.
     *
     * @param tz A Timezone string that is compatible with {@code java.time}.
     */
    void setServerTimeZone(final String tz) {
        this.serverTimeZone = ZoneId.of(tz)
    }

    /** Serializes a date time to a format that PortaOne will understand.
     *
     * @param dt Zoned Date time
     * @return Serialised date time
     */
    String serializeDateTime( final ZonedDateTime dt ) {
        DATETIME_PATTERN.format(dt.withZoneSameInstant(this.serverTimeZone))
    }

    /** Deserializes a date time from PortaOne.
     *
     * @param dt Serialised date time
     * @return Zoned Date time
     */
    ZonedDateTime deserializeDateTime( final String dt ) {
        ZonedDateTime.of(LocalDateTime.parse(dt,DATETIME_PATTERN),this.serverTimeZone)
    }

    /** Sends data to Porta webservice, and returns a result
     *
     * @param path Path to call
     * @param params Parameters to send as per Porta API documentation
     * @return Parsed data as a map.
     * @throw For any non-successfull HTTP response an exception will be thrown.
     */
    @SuppressWarnings('NoDef')
    protected def send( final String path, final Map params ) {
        api.send path, params, sessionInfo
    }

    abstract Map getSessionInfo()
    protected JsonApi api

    private ZoneId serverTimeZone = ZoneId.of('Z')
}
