/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api.client

import digital.iota.porta.api.CustomerXDR
import groovy.transform.CompileStatic
import digital.iota.porta.api.Customers
import digital.iota.porta.api.Invoicing
import digital.iota.porta.api.Session

import java.time.ZonedDateTime

/** A client implementation that implemnts most of the presented interfaces.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class PortaWebserviceClient extends AbstractPortaWebserviceClient
        implements Session, Customers, Invoicing, CustomerXDR {

    /** Instantiates a client.
     *
     * @param baseUri The URI for the PortaOne webapp.
     */
    PortaWebserviceClient(final String baseUri) {
        super(baseUri)
    }

    /** Login and creates a session
     */
    @Override
    void login( final String user, final String password) {

        def result = api.send '/rest/Session/login', [ login : user, password : password ]
        authInfo = [
            login : user,
            session_id : result['session_id']
        ]
    }

    /** Logout and tells remote end to remove the session. Calling logout multiple times will result in NOOPs.
     * @throw Will throw exception if logout fails.
     */
    @Override
    void logout() {
        if (authInfo) {
            send '/rest/Session/logout', [ session_id : authInfo.session_id]
            authInfo = null
        }
    }

    /**
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param offset Number of records to skip
     * @param limit Number of records to return
     * @param processor A closure that will be called for every record returned. it is passed a map-like object
     * with keys matching that of the Porta CustomerInfo structure.
     * @return Number of records processed.
     */
    @Override
    Integer listCustomers(Map properties, int offset, int limit, Closure processor) {
        def result = send '/rest/Customer/get_customer_list', [
            offset : offset,
            limit : limit
        ] + properties

        int count = 0
        result['customer_list'].each {
            ++count
            processor(it)
        }

        count
    }

    /** Lists invoices by customer
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param customerId Customer identifier ({@code i_customer} field in Porta)
     * @param offset Offset to start from
     * @param limit Number of invoices to retrieve
     * @param processor A closure that will be called for every record returned.
     * @return NUmber of invoices processed
     */
    @Override
    Integer listInvoicesByCustomer(Map properties, String customerId, int offset, int limit, Closure processor) {
        listInvoices([
            i_customer : customerId,
            offset : offset,
            limit : limit
        ] + properties,
            processor
        )
    }

    /** Lists invoices by customer
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param resellerId Reseller identifier ({@code i_parent} field in Porta)
     * @param offset Offset to start from
     * @param limit Number of invoices to retrieve
     * @param processor A closure that will be called for every record returned.
     * @return Number of invoices processed
     */
    @Override
    Integer listInvoicesByReseller(Map properties, String resellerId, int offset, int limit, Closure processor) {
        listInvoices ([
            i_parent : resellerId,
            offset : offset,
            limit : limit
            ] + properties,
        processor)
    }

    /** Reads all customer data and pass it to the related processor.
     *
     * NOTE: This will be a long running method.
     *
     * @param properties Additional query properties that can be passed to to Porta.
     *   See {@code get_customer_list} API for details.
     * @param processor A closure that will be called for every record returned. it is passed a map-like object
     * with keys matching that of the Porta CustomerInfo structure.
     * @return Number of records processed.
     */
    @Override
    @SuppressWarnings('SpaceAroundOperator')
    Integer allCustomers(Map properties, Closure processor) {
        final int LIMIT = 100
        int offset = 0
        Integer got = -1
        Integer count = 0
        while (got != 0) {
            got = listCustomers(properties, offset, LIMIT, processor)
            offset+= got
            count+= got
        }
        count
    }

    /**
     * NOTE: According to the PortaONe AP documentation This method has a 40-second time limit.
     * To avoid the 500 Internal Server Error, please use the “offset”, “limit” and “get_total” properties
     * when you need to retrieve large amounts of data.
     *
     * @param properties Additional query properties that can be passed to to Porta.
     *   See {@code get_customer_xdrs} API for details.
     * @param customerID Customer identifier
     * @param fromDateTime Starting date & time for XDR records.
     * @param toDateTime Ending date & time for XDR records.
     * @param offset Offset to start from
     * @param limit Number of invoices to retrieve
     * @param processor A closure that will be called for every record returned.
     * @return Number of XDRs processed.
     */
    @Override
    Integer listXDRsForCustomer(
            Map properties,
            Integer customerID,
            ZonedDateTime fromDateTime,
            ZonedDateTime toDateTime,
            int offset,
            int limit,
            Closure processor) {

        def result = send '/rest/Customer/get_customer_xdrs', [
                i_customer : customerID,
                from_date : serializeDateTime(fromDateTime),
                to_date : serializeDateTime(toDateTime),
                offset : offset,
                limit : limit
        ] + properties

        int count = 0
        result['xdr_list'].each {
            ++count
            processor(it)
        }

        count
    }

    /** Returns the current session info as a map
     *
     * @return Session info. See the PortaOne
     */
    Map getSessionInfo() {
        authInfo
    }

    /** Lists invoices by customer
     *
     * @param properties Combined properties that will sent for the invoice query
     * @param processor A closure that will be called for every record returned.
     * @return Number of invoices processed
     */
    private Integer listInvoices(Map properties, Closure processor) {
        def result = send '/rest/Invoice/get_invoice_list', properties

        int count = 0
        result['invoice_list'].each {
            ++count
            processor(it)
        }

        count

    }

    private Map authInfo

}
