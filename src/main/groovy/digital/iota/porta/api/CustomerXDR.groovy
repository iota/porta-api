/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api

import groovy.transform.CompileStatic

import java.time.ZonedDateTime

/**
 * Interface for dealing with customer XDRs.
 */
@CompileStatic
interface CustomerXDR {

    /** Returns a list of XDRs for a specific customer.
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param customerID Identifier for a specific customer.
     * @param fromDateTime Starting date & time for XDR records.
     * @param toDateTime Ending date & time for XDR records.
     * @param offset Number of records to skip
     * @param limit Number of records to return
     * @param processor A closure that will be called for every record returned.
     * @return Number of records processed.
     */

    Integer listXDRsForCustomer(
            Map properties,
            Integer customerID,
            ZonedDateTime fromDateTime,
            ZonedDateTime toDateTime,
            int offset,
            int limit,
            Closure processor )
}

/*
billing_model
int
Indicates whether the data
should be retrieved for the
credit entries or for
the debit ones:

1 – Credit
accounts.

-1 – Debit
accounts.

Empty – Return
both.
cdr_entity
string
Flag that selects which
xDRs should be returned:

A – Account
xDRs.

C – Customer
xDRs.

Empty – Return
account and
customer xDRs.
for_current_period
int
Specifies whether to show
xDRs for the current
billing period
format
string
This parameter allows API
user to get xDRs in other
formats via SOAP
attachment. Currently only
the “.csv” format is
supported
from_date
dateTime
Get xDRs with bill_time
starting from this date
get_total
ⁿ
int
Get the total number of
the retrieved xDRs
history_pattern
string
Specifies whether to show
xDRs with history that
matches the pattern
i_customer
int
The unique ID of the
customer record
i_invoice
int
Indicates what xDRs will
be shown:

nill – Midterm
xDRs and out-of-
turn xDRs.

0 – Out Of Turn
xDRs.
Porta
Billing
®
Reference
© 2000–2016 PortaOne, Inc. All rights Reserved.  www.portaone.com
113

Not set – xDRs of
all types.
i_service
ⁿ
int
ID of the service; refers to
the Services table
i_service_type
int
The unique ID of
the related service type
limit
ⁿ
int
The number of rows to
retrieve
offset
ⁿ
int
The number of rows to
skip at the beginning of the
list
show_unsuccessful
int
Show xDRs of
unsuccessful attempts
to_date
dateTime
Get xDRs with bill_time
before this date
with_cr_download_links
int
If set, then each xDR will
contain download links to
the recorded files if any
 */
