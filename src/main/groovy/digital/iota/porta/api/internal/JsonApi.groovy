/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api.internal

import java.time.format.DateTimeFormatter

import static groovyx.net.http.ContentType.URLENC

import groovy.json.JsonOutput
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient

/**
 * @author Schalk W. Cronjé
 */
@CompileStatic
@Slf4j
class JsonApi {

    JsonApi(final String baseUri) {
        endpoint = new RESTClient(baseUri)
        configureJsonParser()
    }

    def requestPayload(final Map params, final Map session = null) {
        Map ret = [ params : JsonOutput.toJson(params) ]
        if ( session ) {
            ret['auth_info'] = JsonOutput.toJson([ session_id : session.session_id])
        }
        ret
    }

    /** Parses the encoded JSON data and returns it as a Map
     *
     * @param response
     * @return Map-lie object representing JsonSlurper parsed data
     */
    def responsePayload(final HttpResponseDecorator response) {
        response.data
    }

    /** Send the raw form of the data to Porta, which is JSON encoded in a form-style.
     *
     * @param path Relative base to endpoint
     * @param params Parameters to send accorindg to Porta API
     * @param session Session (auth_info) data if not a login request.
     * @return The decoded data as a Map-like object.
     * @throw If the response in not a 2xx an exception will be thrown
     */
    def send(final String path, final Map params, final Map session = null) {
        HttpResponseDecorator postResult = endpoint.post(
            requestContentType : URLENC,
            path : path,
            body : requestPayload(params, session)
        ) as HttpResponseDecorator

        responsePayload(postResult)
    }

    RESTClient getRestClient() {
        endpoint
    }

    @CompileDynamic
    void configureJsonParser() {
        endpoint.parser.'application/x-www-form-urlencoded' = endpoint.parser.'text/plain'

    }

    private final RESTClient endpoint
}
