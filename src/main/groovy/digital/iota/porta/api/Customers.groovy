/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api

import groovy.transform.CompileStatic

/** Deals with customers in PortaOne
 *
 */
@CompileStatic
interface Customers {

    /** Returns a number of customers from Porta.
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param offset Number of records to skip
     * @param limit Number of records to return
     * @param processor A closure that will be called for every record returned.
     * @return Number of records processed.
     */
    Integer listCustomers( Map properties , int offset, int limit, Closure processor )

    /** Returns all of the customers from Porta.
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param processor A closure that will be called for every record returned.
     * @return Number of records processed.
     */
    Integer allCustomers(Map properties, Closure processor)

}
