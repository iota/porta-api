/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015-2016, Iota S.L. 2017
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package digital.iota.porta.api

import groovy.transform.CompileStatic

/**
 *
 */
@CompileStatic
interface Invoicing {

    /** Lists invoices by customer
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param customerId Customer identifier ({@code i_customer} field in Porta)
     * @param offset Offset to start from
     * @param limit Number of invoices to retrieve
     * @param processor A closure that will be called for every record returned.
     * @return NUmber of invoices processed
     */
    Integer listInvoicesByCustomer( Map properties, final String customerId, int offset, int limit, Closure processor)

    /** Lists invoices by customer
     *
     * @param properties Additional query properties that can be passed to to Porta. See API for details
     * @param resellerId Reseller identifier ({@code i_parent} field in Porta)
     * @param offset Offset to start from
     * @param limit Number of invoices to retrieve
     * @param processor A closure that will be called for every record returned.
     * @return NUmber of invoices processed
     */
    Integer listInvoicesByReseller( Map properties, final String resellerId, int offset, int limit, Closure processor)

}
