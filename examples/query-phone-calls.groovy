@Grab(group='digital.iota', module='porta-api', version='0.2-SNAPSHOT')
@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1')
@Grab(group='org.slf4j', module='slf4j-simple', version='1.7.5')
import digital.iota.porta.api.client.PortaWebserviceClient

String url = args[0]
String user = args[1]
String password = args[2]

final String resellerName = 'Creditel Business Solutions (CRE004)'

PortaWebserviceClient client = new digital.iota.porta.api.client.PortaWebserviceClient(url)

try {
    client.login(user,password)

//    client.listCustomers( name : resellerName,0,1) { reseller ->
//        println "*** ${resellerName}: ${reseller['i_customer']}"
//
//        client.listCustomers( i_parent : reseller['i_customer'], 0, 100 ) { customer ->
//            println "   *** ${customer['name']} : ${customer['i_customer']}"
//        }
//    }

    client.listXDRsForCustomer(  4231,0, 10) { xdr ->
        println "*** ${xdr}"
    }
} finally {
    client.logout()
}